#!/bin/bash
MUSER="root"
MPASS="admin"
MHOST="localhost"
TOKEN="MtrGSegACSa0dfvlzk4MZ7e0GKHnHuvzWqbLqeJw6e3"
BACKUP=/var/backup
TAR="$(which tar)"
GZIP="$(which gzip)"
RSYNC="$(which rsync)"
MYSQL="$(which mysql)"
MYSQLDUMP="$(which mysqldump)"
CURL="$(which curl)"
TAR="$(which tar)"
MAIL="$(which mail)"
CAT="$(which cat)"
TRUNCATE="$(which truncate)"
NOW=$(date +%Y-%m-%d)
TIME=$(date +'%Y-%m-%d %H:%M:%S')
DATE=`date '+_%Hh%Mm_%aday'`
REPORT=/var/report.txt
LOG_DUMP_ERROR=/tmp/mysqldump_error.log
LOG_MYSQL_ERROR=/tmp/mysql_error.log
LOG_RSYNC=/tmp/rsync
LOG_REPORT=/var/log/backupdb/log_report.txt
mkdir -p  $BACKUP/$NOW
mkdir -p /var/log/backupdb

#Style
RED="\033[0;31m"
GREEN="\033[0;32m"

#Chack User Account 

USER=$(cut -d: -f1 /etc/passwd | grep bak)
#echo $USER
if  [ -z "$USER"  ]
then
    useradd bak; echo -e "Mono@mvas\nMono@mvas" | passwd bak    
else
    echo "user already exists"
fi

MYSQL_VERSION="$($MYSQL -V | awk '{print $3}')"
MYSQL_VERSION=${MYSQL_VERSION%.*}
echo $'\n' >> $REPORT
echo "### Backup Start Time: $NOW " $'\n' >> $REPORT 
echo "### Hostname: $(hostname) " $'\n' >> $REPORT 
echo "### Backup directory: $BACKUP  " $'\n' >> $REPORT 
echo "======================================" >> $REPORT 
$TRUNCATE -s 0 $LOG_REPORT

if  mysql -u root -p$MPASS  >> $LOG_MYSQL_ERROR 2>&1 -e ";"; then
  if [ "$MYSQL_VERSION" -ge  15 ] 
    then
        DBS="$($MYSQL -u $MUSER -h $MHOST -p$MPASS -Bse 'show databases' )"
    else
        DBS="$($MYSQL --defaults-extra-file=$HOME/.my.cnf -Bse 'show databases' )"
  fi

  for db in $DBS
  do
  mkdir -p  $BACKUP/$NOW/$db
  Start_TIME="$(date +'%Y-%m-%d %H:%M:%S')"
  echo "---------------------------------------------------------" >> $REPORT
  echo "Backup of Database ( $db ) "  >> $REPORT 
  
      if [ "$MYSQL_VERSION" -ge  15 ] 
      then
          TOTAL=$($MYSQL -u $MUSER -h $MHOST -p$MPASS -Bse "SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = '$db';")
      else
          TOTAL=$($MYSQL  --defaults-extra-file=$HOME/.my.cnf  -Bse "SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = '$db';")
      fi
count_ok=0
count_error=0
  for i in `echo "show tables" | $MYSQL -u $MUSER -h $MHOST -p$MPASS $db|grep -v Tables_in_`;
  do
      
      FILE=$BACKUP/$NOW/$db/$i.sql
      if [ "$MYSQL_VERSION" -ge  15 ]   
      then
          echo $i; $MYSQLDUMP --add-drop-table --allow-keywords --single-transaction -q -c -u $MUSER -h $MHOST -p$MPASS $db $i 2>> $LOG_DUMP_ERROR  > $FILE
      else
          echo $i; $MYSQLDUMP  --defaults-extra-file=$HOME/.my.cnf --single-transaction -q -c   $db $i 2>> $LOG_DUMP_ERROR  > $FILE
      fi

      if [ "$?" -eq 0 ]
      then  
          
          $GZIP -f -9  $FILE
          count_ok=$((count_ok+1))
      else
          echo "$db --> $count_error"
          count_error=$((count_error+1))
          table_error="$i" 
          MYSQLDUMP_ERROR_CODE="`cat $LOG_DUMP_ERROR`"
          echo "$MYSQLDUMP_ERROR_CODE" $'\n' >> $REPORT
          echo "-------------------------------------------" >> $REPORT
      fi
      done
          END_TIME="$(date +'%Y-%m-%d %H:%M:%S')"
          echo  "$TOTAL Tables in Total,$count_error Error, $count_ok Success Start $Start_TIME  End $END_TIME" >> $REPORT 
          echo  "Backup of Database ($db),$TOTAL Tables in Total,$count_error Error, $count_ok Success ,Start $Start_TIME , End $END_TIME" >> $LOG_REPORT
          chown -R bak:bak $LOG_REPORT

done



echo "======================================"  $'\n'>> $REPORT
echo "Backup End" >> $REPORT




#$CURL https://notify-api.line.me/api/notify -H "Authorization: Bearer ${TOKEN}" -d "message=${line}"

#COPY  BACKUP TO SERVER BACKUP

RSYNC_SERVER=( "192.168.30.35" "192.168.3.220")
RSYNC_MODULES=("MY_124" "MY_124")
FILE_DATE=$BACKUP/$NOW
cd $BACKUP
$TAR -zcvf $FILE_DATE.tar.gz --remove-files  $NOW
SOUCRCE_SIZE="`du -sh $BACKUP/$NOW.tar.gz | cut -c 1-4`"

for ((i = 0; i < ${#RSYNC_SERVER[@]} && i < ${#RSYNC_MODULES[@]}; i++))
do
    $RSYNC -avz --no-o --no-g  --size-only  -h  --log-file=$LOG_RSYNC-$i.log  $BACKUP/$NOW.tar.gz  ${RSYNC_SERVER[i]}::${RSYNC_MODULES[i]}
            if [ "$?" -eq "0" ]
            then
            echo "Done."
            RSYNC_LOG="`cat $LOG_RSYNC-$i.log | sed -n -e 's/^.*\( size is \)/\1/p' | sed s/size/SIZE/`"
            echo  "#### Sync Server IP :${RSYNC_SERVER[i]}:${RSYNC_MODULES[i]} Done. #####" $'\n' >> $REPORT 
            echo "======================================" $'\n' >> $REPORT 
            echo "SOURCE  : SIZE is  $SOUCRCE_SIZE" $'\n' >> $REPORT
            echo "DESTINATION : $RSYNC_LOG" $'\n' >> $REPORT
            echo "======================================" $'\n' >> $REPORT 

            else
            RSYNC_LOG="`cat $LOG_RSYNC-$i.log `"
            echo  "#### Sync Server Error. IP : ${RSYNC_SERVER[i]}:${RSYNC_MODULES[i]} #####" $'\n' >> $REPORT 
            echo "======================================" $'\n' >> $REPORT 
            echo "$RSYNC_LOG" $'\n' >> $REPORT
            echo "======================================" $'\n' >> $REPORT 

            fi
            $TRUNCATE -s 0  $LOG_RSYNC-$i.log
            
done
REPORT_MAIL="`cat $REPORT`"

if  [ "$count_error" -eq 0 ] 
    then 
       echo "MAIL OK"
       echo "$REPORT_MAIL"  | $MAIL -s "Backup of Database : $(hostname) - วันที่ $NOW  "  -r network_vas@mono.co.th network_vas@mono.co.th
  else
       echo "MAIL FAIL"
       echo "$MYSQLDUMP_ERROR_CODE"  | $MAIL -s "Backup of Database : $(hostname) (Table Error) - วันที่ $NOW"  -r network_vas@mono.co.th  network_vas@mono.co.th
       $CURL https://notify-api.line.me/api/notify -H "Authorization: Bearer ${TOKEN}" -d "message=${MYSQLDUMP_ERROR_CODE}"
       
fi
$TRUNCATE -s 0  $REPORT
rm -rf $BACKUP/$NOW.tar.gz
$TRUNCATE -s 0  $LOG_DUMP_ERROR
$TRUNCATE -s 0  $LOG_MYSQL_ERROR
else

    echo "Can't Connect Databases: $(hostname)" $'\n' >> $REPORT
    mysql_error_code="`cat $LOG_MYSQL_ERROR`"
    echo "$mysql_error_code" $'\n' >> $REPORT
    REPORT_MAIL="`cat $REPORT`"
    echo "$REPORT_MAIL"  | $MAIL -s "Backup of Database : $(hostname) ERROR Can't Connect Databases - วันที่ $NOW  "  -r network_vas@mono.co.th network_vas@mono.co.th
    $TRUNCATE -s 0  $REPORT
    $TRUNCATE -s 0  $LOG_DUMP_ERROR
    $TRUNCATE -s 0  $LOG_MYSQL_ERROR

fi
